import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var candleImageView: UIImageView!
    @IBOutlet weak var fire: UIImageView!
    @IBOutlet weak var fireSizeLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var startTimerButton: UIButton!
    
    @IBOutlet weak var fireTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var fireHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var fireWidthConstraint: NSLayoutConstraint!
    
    var animationArray: [UIImage]?
    var countdownTimer: Timer!
    var totalMaxTime = 60
    
    var startIndex = 1
    var endIndex = 10
    var fireSize = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        createFire()
        timerStart()
    }
    
    func initialSetup() {
        startTimerButton.isEnabled = false
        fireSizeLabel.text = "Fire Size: \(fireSize)"
        animationArray = [UIImage]()
        
        for i in 0...49 {
            let imgName = "\(i)"
            guard let image = UIImage(named: imgName) else { return }
            
            animationArray?.append(image)
        }
    }
    
    func timerStart() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1,
                                              target: self,
                                              selector: #selector(updateTime),
                                              userInfo: nil,
                                              repeats: true)
    }

    @objc func updateTime() {
        timerLabel.text = "\(timeFormatted(totalMaxTime))"
        startTimerButton.isEnabled = false
        if totalMaxTime != 0 {
            totalMaxTime -= 1
        } else {
            endTimer()
            startTimerButton.isEnabled = true
            fire.isHidden = true
        }
    }

    func endTimer() {
        countdownTimer.invalidate()
    }
    
    func scaleUpFire() {
        fireTopConstraint.constant -= 4
        fireHeightContraint.constant += 6
        fireWidthConstraint.constant += 2
        view.layoutIfNeeded()
    }
    
    func scaleDownFire() {
        fireTopConstraint.constant += 4
        fireHeightContraint.constant -= 6
        fireWidthConstraint.constant -= 2
        view.layoutIfNeeded()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    func createFire() {
        fire.contentMode = .scaleAspectFit
        fire.animationImages = animationArray
        fire.animationDuration = 1
        fire.animationRepeatCount = 0
        fire.startAnimating()
    }
    
    @IBAction func minusButtonAction(_ sender: Any) {
        if fireSize >= startIndex {
            fireSize -= 1
            fireSizeLabel.text = "Fire Size: \(fireSize)"
            scaleDownFire()
            fire.isHidden = false
            view.layoutIfNeeded()
        } else if fireSize == 0 {
            fire.isHidden = true
            view.layoutIfNeeded()
        }
    }
    
    @IBAction func plusButtonAction(_ sender: Any) {
        fire.isHidden = false
        if fireSize < endIndex {
            fireSize += 1
            scaleUpFire()
            fireSizeLabel.text = "Fire Size: \(fireSize)"
        }
    }
    
    @IBAction func startTimerAction(_ sender: Any) {
        totalMaxTime = 60
        fire.isHidden = false
        fireSize = 1
        timerStart()
    }
}
